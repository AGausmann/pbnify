# PBNify

Converts images into commands for dwangoAC's Paint By Numbers, with some
built-in image manipulation utilities.

This crate has a command-line interface for humans and scripts (documented here)
and an API for Rust programs ([external docs](https://docs.rs/pbnify)).

## Installation

The latest release of crate is published on crates.io, making it easy to
run if you have [Rust](https://rust-lang.org) installed:

```
$ cargo install pbnify
```

If you want to test/develop on the latest master branch, you can build it with
Cargo, and then run with `$ cargo run -- ...` instead of `$ pbnify ...`:

```
$ git clone https://gitlab.com/agausmann/pbnify.git
$ cd pbnify
$ cargo build
```

## Basic Usage

To convert an image's raw data into PBN commands:

```
$ pbnify -i my_image.png
#000000ff 1,1;1,2;2,1;
...
```

To move the top-left corner of the image to a different location:

```
$ pbnify -i my_image.png -x 40 -y 30
...
```

To scale the given image before generating PBN:

```
$ pbnify -i my_image.png -w 132 -H 99
...
```

To quantize an image (reduce the number of unique colors; in this case, to 16):

```
$ pbnify -i my_image.png -n 16
...
```

## Full Usage

```
USAGE:
    pbnify [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -H, --height <HEIGHT>     Rescales the picture to HEIGHT pixels tall. Will preserve the original ratio unless
                              --width is also provided.
    -i, --input <FILE>        Tells the program to accept the image from FILE. If this option is not present, standard
                              input will be read and PNG is assumed.
    -l, --max-length <NUM>    Sets the maximum length of a command. [default: 400]
    -o, --output <FILE>       Tells the program to write the generated output to FILE. If this option is not present, it
                              will be written to standard output.
    -n, --quantize <SIZE>     Generates a palette of size SIZE, and quantizes the input image before PBNification.
    -w, --width <WIDTH>       Rescales the picture to WIDTH pixels wide. Will preserve the original ratio unless
                              --height is also provided.
    -x, --x-offset <NUM>      Offsets the output x coordinates by the given amount. [default: 0]
    -y, --y-offset <NUM>      Offsets the output y coordinates by the given amount. [default: 0]
```

## License

Copyright (C) 2019  Adam Gausmann

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
